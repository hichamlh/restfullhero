The goal of this assigment is to have a REST application to manage a Super hero company. 
The application needs to be developped with [Java](https://www.java.com) and [Spring Boot](http://spring.io/projects/spring-boot).

### Features:
- Operations:
    - Create a super hero
    - Read a super hero info also, show his active missions and completed missions
    - Update a super Hero info and add/remove available missions. (Unable to remove a completed mission)
    - Delete a super Hero
    - Create a new mission
    - Read missions information
    - Update a mission
    - delete a mission
    - All operations must be accessible via a restful API with [Swagger](https://swagger.io/)
    - The application build with [maven](https://maven.apache.org/)
    - Use spring Repository to Create/read/update/delete the data (in a [H2](http://www.h2database.com) db for example)


### Data model:
- Super Hero
    - String Firstname
    - String Lastname
    - String Superheroname
    - List   Missions
- Mission
    - String  MissionName
    - Boolean IsCompleted
    - Boolean Isdeleted
    - List    Heroes
    
    
### Link :
	- https://bitbucket.org/hichamlh/restfullhero/get/3347ee42aac3.zip
	- http://localhost:8080/swagger-ui.html