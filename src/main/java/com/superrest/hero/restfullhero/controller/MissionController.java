package com.superrest.hero.restfullhero.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.superrest.hero.restfullhero.model.Mission;
import com.superrest.hero.restfullhero.preload.MissionRequest;
import com.superrest.hero.restfullhero.preload.MissionResponse;
import com.superrest.hero.restfullhero.service.MissionService;

@RestController
@RequestMapping("/api/missions")
public class MissionController {
	
	
	private MissionService missionService;
	
	
	@Autowired
	public MissionController(MissionService missionService) {
		super();
		this.missionService = missionService;
	}

	@GetMapping
	public List<Mission> MissionfindAll(){
		return missionService.findAllMissions();
	}



	@GetMapping("/{missionId}")
	public MissionResponse MissionById(@PathVariable Long missionId) {
		Mission mission =  missionService.findMissionById(missionId);
		return new MissionResponse(mission.getMissionname(),mission.isIscompleted(),mission.isIsdeleted(),
				mission.getHeroes());
	}
	
	@PostMapping
	public Mission creatMission(@RequestBody MissionRequest mission) {
		return missionService.creatMission(mission);
	}
	
	@PostMapping("update/{missionId}")
	public Mission UpdateMission(@PathVariable Long missionId, @RequestBody MissionRequest missionrequest) {
		return missionService.UpdateMission(missionId, missionrequest);
		
	}
	
	@DeleteMapping("/delete/{missionId}")
	public Mission DeleteMission(@PathVariable Long missionId) {
		return missionService.DeleteMission(missionId);
	}

}
