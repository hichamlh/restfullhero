package com.superrest.hero.restfullhero.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.superrest.hero.restfullhero.model.SuperHero;
import com.superrest.hero.restfullhero.preload.Message;
import com.superrest.hero.restfullhero.preload.MissionRequest;
import com.superrest.hero.restfullhero.preload.SuperHeroRequest;
import com.superrest.hero.restfullhero.preload.SuperheroMissionRequest;
import com.superrest.hero.restfullhero.preload.SuperheroResponse;
import com.superrest.hero.restfullhero.service.SuperHeroService;

@RestController
@RequestMapping("/api/superheroes")
public class SuperHeroController {
	
	
	private SuperHeroService superHeroservice;
	
	@Autowired
	public SuperHeroController(final SuperHeroService superheroService) {
		this.superHeroservice = superheroService;
	}
	
	@GetMapping
	public List<SuperHero> SuperHerofindAll() {
		return superHeroservice.SuperHerofindAll();
	}
	
	@GetMapping("/{superheroId}")
	public SuperheroResponse SuperheroById(@PathVariable Long superheroId) {
		SuperHero superhero= superHeroservice.SuperHerofindById(superheroId);
		 return new SuperheroResponse(superhero.getFirstname(),superhero.getLastname(),superhero.getSuperheroname(),
				 superhero.getMissions());
	}
	
	@PostMapping
	public SuperHero CreatSuperHero(@RequestBody SuperHeroRequest superheroRequest) {
		return superHeroservice.CreatSuperHero(superheroRequest);
	}
	
	
	@PostMapping("/addHeroToMission")
	public Message addSuperHeroToMission(@RequestBody SuperheroMissionRequest superheromissionrequest) {
		return superHeroservice.addSuperHeroToMission(superheromissionrequest);
	}
	
	@PutMapping("/update/{superheroId}")
	public SuperHero UpdateSuperHero(@PathVariable Long superheroId, @Valid @RequestBody SuperHeroRequest superheroRequest) {
		return superHeroservice.UpdateSuperHero(superheroId, superheroRequest);
	}

	@DeleteMapping("/delete/{superheroId}")
	public Message DeleteSuperHero(@PathVariable Long superheroId) {
		return superHeroservice.DeleteSuperHero(superheroId);		  
	}
	
	@DeleteMapping("/{superheroId}/{missionId}")
	public Message DeleteMissionFromSuperHero(@PathVariable Long superheroId, @PathVariable Long missionoId) {
		
		return superHeroservice.deleteMissionFromSuperHero(superheroId, missionoId);
		
	}
	
	
	
	
}
