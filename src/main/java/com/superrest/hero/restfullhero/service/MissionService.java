package com.superrest.hero.restfullhero.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.superrest.hero.restfullhero.exception.ResourceNotFoundException;
import com.superrest.hero.restfullhero.model.Mission;
import com.superrest.hero.restfullhero.preload.MissionRequest;
import com.superrest.hero.restfullhero.repository.MissionRepository;

@Service
public class MissionService {

	
	private  MissionRepository missionrepository;

	@Autowired
	public MissionService(MissionRepository missionrepository) {
		//super();
		this.missionrepository = missionrepository;
	}


	public List<Mission> findAllMissions(){
		
		return missionrepository.findAll();
	}
	
	
	public Mission findMissionById(Long id) {
		
		return missionrepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Mission","ID", id));
		
	}
	
	public Mission creatMission(MissionRequest mission) {
		
		Mission missions = new Mission();
		missions.setMissionname(mission.getMissionName());
		missions.setIscompleted(mission.getIsCompleted());
		missions.setIsdeleted(mission.getIsdeleted());
		
		return missionrepository.save(missions);
		
	}
	
	public Mission UpdateMission(Long missionid, MissionRequest missionrequest) {
		
		return missionrepository.findById(missionid).map(mission -> {
			mission.setMissionname(missionrequest.getMissionName());
			mission.setIscompleted(missionrequest.getIsCompleted());
			mission.setIsdeleted(missionrequest.getIsdeleted());
			return missionrepository.save(mission);
		}).orElseThrow(()-> new ResourceNotFoundException("Mission","ID ", missionid) );
		
		
	}
	
	public Mission DeleteMission(Long missionId) {
		
		return missionrepository.findById(missionId).map(mission ->{
			mission.setIsdeleted(true);
			return missionrepository.save(mission);
		}).orElseThrow(()-> new ResourceNotFoundException("mission","id",missionId));
				
		
	}
	
	
	
}
