package com.superrest.hero.restfullhero.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.superrest.hero.restfullhero.exception.ResourceNotFoundException;
import com.superrest.hero.restfullhero.model.Mission;
import com.superrest.hero.restfullhero.model.SuperHero;
import com.superrest.hero.restfullhero.preload.Message;
import com.superrest.hero.restfullhero.preload.SuperHeroRequest;
import com.superrest.hero.restfullhero.preload.SuperheroMissionRequest;
import com.superrest.hero.restfullhero.repository.SuperHeroRepository;

@Service
public class SuperHeroService {
	
    
	private  SuperHeroRepository superHerorepository;
	private  MissionService missionservice;
	
	@Autowired
	public SuperHeroService(final SuperHeroRepository superHerorepository,MissionService missionservice) {
		//super();
		this.superHerorepository = superHerorepository;
		this.missionservice = missionservice;
	}

	// find all superhero
	public List<SuperHero> SuperHerofindAll() {
		return superHerorepository.findAll();
	}

	// find superhero
	public SuperHero SuperHerofindById(Long superhero_id) {
		return superHerorepository.findById(superhero_id)
		.orElseThrow(()-> new ResourceNotFoundException("SuperHero","ID",superhero_id));

	}
	
	// New superhero
	public SuperHero CreatSuperHero(SuperHeroRequest Requestsuperhero) {
		
		SuperHero superhero = new SuperHero();
		superhero.setFirstname(Requestsuperhero.getFirstname());
		superhero.setLastname(Requestsuperhero.getLastname());
		superhero.setSuperheroname(Requestsuperhero.getSuperheroname());
		
		return superHerorepository.save(superhero);
		
	}
	
	// Update superhero
	public SuperHero UpdateSuperHero(Long superheroId, SuperHeroRequest Requestsuperhero) {
		
		return superHerorepository.findById(superheroId).map(superhero -> {
			superhero.setFirstname(Requestsuperhero.getFirstname());
			superhero.setLastname(Requestsuperhero.getLastname());
			superhero.setSuperheroname(Requestsuperhero.getSuperheroname());			
			return superHerorepository.save(superhero);
		}).orElseThrow(() -> new ResourceNotFoundException("Superhero", "id", superheroId));		
	}
	
	public Message addSuperHeroToMission(SuperheroMissionRequest superheromissionrequest) {
		
		//Find Mission
		Mission mission = missionservice.findMissionById(superheromissionrequest.getIdMission());
		//Find superhero
		SuperHero superhero = this.SuperHerofindById(superheromissionrequest.getIdSuperHero());

		
		superhero.addMission(mission);
		superHerorepository.save(superhero);
		
		return new Message(true,"MissionAdd to SuperHero");
	}
	
	
	
	// Delete superhero
	public Message DeleteSuperHero(Long superhero_id) {
		
		return superHerorepository.findById(superhero_id).map(superdelete ->{
			superHerorepository.delete(superdelete);
			return new Message(true,"Superhero deleted");
		}).orElseThrow(() -> new ResourceNotFoundException("Superhero", "id", superhero_id));
	}
	
	
	// Delete mission from superhero
	public Message deleteMissionFromSuperHero(Long superhero_id,Long mission_id) {
		
		//Find superhero
		SuperHero superhero = SuperHerofindById(superhero_id);
		//Find Mission
		Mission mission = missionservice.findMissionById(mission_id);
		
//		if(mission.isCompleted()) {
//			return new Message(false, "Unable to remove a completed mission");
//		}
		
		superhero.removeMission(mission);
		superHerorepository.save(superhero);
		return new Message(true, "Mission removed from Superhero");		
	}
	
	

	

}
