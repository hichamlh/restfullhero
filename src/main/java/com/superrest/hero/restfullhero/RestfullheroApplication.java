package com.superrest.hero.restfullhero;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestfullheroApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestfullheroApplication.class, args);
	}

}
