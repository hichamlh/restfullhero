package com.superrest.hero.restfullhero.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.superrest.hero.restfullhero.model.SuperHero;


@Repository
@Transactional
public  interface SuperHeroRepository extends JpaRepository<SuperHero,Long>{
		
}