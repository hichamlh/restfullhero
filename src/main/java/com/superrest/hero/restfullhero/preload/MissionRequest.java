package com.superrest.hero.restfullhero.preload;

public class MissionRequest {
	
	private String  MissionName;
    private boolean IsCompleted;
    private boolean Isdeleted;
	
    
    public MissionRequest() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MissionRequest(String missionName, boolean isCompleted, boolean isdeleted) {
		super();
		MissionName = missionName;
		IsCompleted = isCompleted;
		Isdeleted = isdeleted;
	}

	public String getMissionName() {
		return MissionName;
	}

	public void setMissionName(String missionName) {
		MissionName = missionName;
	}

	public boolean getIsCompleted() {
		return IsCompleted;
	}

	public void setIsCompleted(boolean isCompleted) {
		IsCompleted = isCompleted;
	}

	public boolean getIsdeleted() {
		return Isdeleted;
	}

	public void setIsdeleted(boolean isdeleted) {
		Isdeleted = isdeleted;
	}
    
    

}
