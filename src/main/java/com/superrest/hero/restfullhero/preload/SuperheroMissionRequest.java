package com.superrest.hero.restfullhero.preload;

public class SuperheroMissionRequest {
	
	private Long idSuperHero;
	private Long idMission;
	
	
	
	public SuperheroMissionRequest() {
		super();
	}

	public SuperheroMissionRequest(Long idSuperHero, Long idMission) {
		super();
		this.idSuperHero = idSuperHero;
		this.idMission = idMission;
	}

	public Long getIdSuperHero() {
		return idSuperHero;
	}

	public void setIdSuperHero(Long idSuperHero) {
		this.idSuperHero = idSuperHero;
	}

	public Long getIdMission() {
		return idMission;
	}

	public void setIdMission(Long idMission) {
		this.idMission = idMission;
	}
	
	
	

}
