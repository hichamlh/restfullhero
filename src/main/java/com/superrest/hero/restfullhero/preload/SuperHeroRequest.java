package com.superrest.hero.restfullhero.preload;

public class SuperHeroRequest {
	
	private String Firstname;
    private String Lastname;
    private String Superheroname;
    
	public SuperHeroRequest(String firstname, String lastname, String superheroname) {
		super();
		Firstname = firstname;
		Lastname = lastname;
		Superheroname = superheroname;
	}
	
	public String getFirstname() {
		return Firstname;
	}
	public void setFirstname(String firstname) {
		Firstname = firstname;
	}
	public String getLastname() {
		return Lastname;
	}
	public void setLastname(String lastname) {
		Lastname = lastname;
	}
	public String getSuperheroname() {
		return Superheroname;
	}
	public void setSuperheroname(String superheroname) {
		Superheroname = superheroname;
	}
    
    

}
