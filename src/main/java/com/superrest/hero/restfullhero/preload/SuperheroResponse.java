package com.superrest.hero.restfullhero.preload;

import java.util.ArrayList;
import java.util.List;

import com.superrest.hero.restfullhero.model.Mission;

public class SuperheroResponse {
	
	private String firstName;
    private String lastName;
    private String superheroName;
    
    private  List <Mission> missions = new ArrayList<>();

	public SuperheroResponse(String firstName, String lastName, String superheroName, List<Mission> missions) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.superheroName = superheroName;
		this.missions = missions;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSuperheroName() {
		return superheroName;
	}

	public void setSuperheroName(String superheroName) {
		this.superheroName = superheroName;
	}

	public List<Mission> getMissions() {
		return missions;
	}

	public void setMissions(List<Mission> missions) {
		this.missions = missions;
	}
}
