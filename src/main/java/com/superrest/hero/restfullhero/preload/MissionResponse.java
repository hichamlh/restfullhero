package com.superrest.hero.restfullhero.preload;

import java.util.ArrayList;
import java.util.List;

import com.superrest.hero.restfullhero.model.SuperHero;

public class MissionResponse {
	
	private String  MissionName;
    private boolean IsCompleted;
    private boolean Isdeleted;
    private List <SuperHero> superheroes = new ArrayList<>();
    
    
	public MissionResponse() {
		super();
	}


	public MissionResponse(String missionName, boolean isCompleted, boolean isdeleted, List<SuperHero> superheroes) {
		super();
		MissionName = missionName;
		IsCompleted = isCompleted;
		Isdeleted = isdeleted;
		this.superheroes = superheroes;
	}


	public String getMissionName() {
		return MissionName;
	}


	public void setMissionName(String missionName) {
		MissionName = missionName;
	}


	public boolean getIsCompleted() {
		return IsCompleted;
	}


	public void setIsCompleted(boolean isCompleted) {
		IsCompleted = isCompleted;
	}


	public boolean getIsdeleted() {
		return Isdeleted;
	}


	public void setIsdeleted(boolean isdeleted) {
		Isdeleted = isdeleted;
	}


	public List<SuperHero> getsuperheroes() {
		return superheroes;
	}


	public void setHeroes(List<SuperHero> superheroes) {
		this.superheroes = superheroes;
	}
    
	
    

}
