package com.superrest.hero.restfullhero.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="SuperHero" )
public class SuperHero {
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(name = "firstname")
	private String firstname;
	
	@Column(name = "lastname")
    private String lastname;
	
	@Column(name = "Superheroname")
    private String Superheroname;
	
	@ManyToMany(cascade = {CascadeType.ALL},fetch = FetchType.LAZY)
	@JoinTable(
	        name = "SuperHero_Mission", 
	        joinColumns = {@JoinColumn(name = "SuperHero_id", referencedColumnName="Id")}, 
	        inverseJoinColumns = {@JoinColumn(name = "missions_id", referencedColumnName="Id")}
	    )
    private  List <Mission> missions = new ArrayList<>();
	
	
	
    
	public SuperHero() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SuperHero(String string) {
		super();
		// TODO Auto-generated constructor stub
	}

	public SuperHero(Long id, String firstname, String lastname, String superheroname, List<Mission> missions) {
		super();
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.Superheroname = superheroname;
		this.missions = missions;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getSuperheroname() {
		return Superheroname;
	}

	public void setSuperheroname(String superheroname) {
		Superheroname = superheroname;
	}

	public List<Mission> getMissions() {
		return missions;
	}

	public void setMissions(List<Mission> missions) {
		this.missions = missions;
	}
    
    
	public void addMission(Mission mission) {
	    this.missions.add(mission);
    }
	
	public void removeMission(Mission mission) {
	    this.missions.remove(mission);
    }
    
    

}
