package com.superrest.hero.restfullhero.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="missions")
public class Mission {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="missionname")
    private String  missionname;
	
	//@Column(name="iscompleted")
    private boolean iscompleted;
	
	//@Column(name= "isdeleted")
    private boolean isdeleted;
	
	
	@ManyToMany(fetch = FetchType.LAZY, cascade = {	
			CascadeType.PERSIST,
			CascadeType.MERGE
			},mappedBy = "missions")
	@JsonIgnore
    private List<SuperHero> heroes = new ArrayList<>();

	
	

	
    public Mission() {
		super();
		// TODO Auto-generated constructor stub
	}



	public Mission(String missionname, boolean iscompleted, boolean isdeleted) {
		super();
		this.missionname = missionname;
		this.iscompleted = iscompleted;
		this.isdeleted = isdeleted;
	}



	public Mission(Long id, String missionname, boolean iscompleted, boolean isdeleted) {
		super();
		this.id = id;
		this.missionname = missionname;
		this.iscompleted = iscompleted;
		this.isdeleted = isdeleted;
	}



	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public String getMissionname() {
		return missionname;
	}



	public void setMissionname(String missionname) {
		this.missionname = missionname;
	}



	public boolean isIscompleted() {
		return iscompleted;
	}



	public void setIscompleted(boolean iscompleted) {
		this.iscompleted = iscompleted;
	}



	public boolean isIsdeleted() {
		return isdeleted;
	}



	public void setIsdeleted(boolean isdeleted) {
		this.isdeleted = isdeleted;
	}



	public List<SuperHero> getHeroes() {
		return heroes;
	}



	public void setHeroes(List<SuperHero> heroes) {
		this.heroes = heroes;
	}



	public void addSuperhero(SuperHero superhero) {
	    this.heroes.add(superhero);
    }
	
	/////
	
	
	
	
	

    
   
}
